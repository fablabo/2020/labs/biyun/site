# 1.  Student's note

| Name                           | Alex                                        | Izzy                                  | Allen                           | Max                            | Richard                         |
|--------------------------------|---------------------------------------------|---------------------------------------|---------------------------------|--------------------------------|---------------------------------|
| Link                           | https://keyunhao.github.io/index.html       | https://killmymemory.github.io/       | https://allen-bit.github.io/    | https://maxdalord.github.io/   | https://c6h806.github.io/       |
| 1. Web development             | Git   documentation is missing              | Passed                                | HTML documentation is missing   | Git documentation is missing   | HTML documentation is missing   |
|                                |                                             |                                       | Git documentation is misssing   |                                | Git documentation is misssing   |
| 2. 2D Design and Laser Cutting |                                             |                                       |                                 |                                |                                 |
|                                |                                             |                                       |                                 |                                |                                 |
| 3. 3D CAD and 3D Printing      | Documentation is missing                    | Documentation is missing              | Documentation is missing        | Documentation is missing       | Documentation is missing        |
|                                |                                             |                                       |                                 |                                |                                 |
| 4. Input Device                | Picture of the working project is missing   |                                       | Documentation is missing        | Pictures is missing            | Passed                          |
|                                |                                             |                                       |                                 |                                |                                 |
| 5. Output Device               | Picture of the working project is missing   | Image is   missing                    | Documentation is missing        | Incorrect download link        | Passed                          |
|                                |                                             | code and   download link are missing. |                                 |                                |                                 |
| Final Project                  |                                             |                                       |                                 |                                |                                 |
