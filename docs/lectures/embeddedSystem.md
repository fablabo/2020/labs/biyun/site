# 3. Embedded Systems

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Objectives

-   Definitions of embedded systems
-   How to read input from a pushbutton
-   How to switch LED On when button is pressed


##  Arduino IDE
https://www.arduino.cc/en/main/software

The Arduino Software (IDE) allows you to write programs and upload them to your board.



## Definition
Embedded system is a very simple computer, it can process only electric signals (a bit like the electric pulses that are sent between neurons in our brains). 


## Examples

## Input devices
An input device is an hardware device able to produce an electrical signal depending on a physical input. The electrical signal changes accordingly to the changes of the input and can be read from a computer. All the sensors can be considered input devices, but also a camera, a keyboard, a mouse and so on. Due to the presence of an analog-digital converter and dedicated functions a microcontroller is well suited to directly read data from sensors.

## Output devices
An output device is an hardware device able to produce a physical output given a specific electrical signal. The output changes accordingly to the electrical signal in input, that can be generated from a computer. Any kind of electrical actuators can be considered as output devices but also a printer, a speaker and so on. A microcontroller is able to generate the proper electrical signals to control a wide range of output devices.


## Installing Git SCM

## Create a new GitHub Project

Open up the GitHub Desktop app and click the “Create New Repository” button.

When the “Create a New Repository” dialog window appears, fill in the “Name” text input as:

Replace [username] with your GitHub account username.

##  Create Repository

##  Publish your website
https://[username].github.io/


</div>
