# 4. Computional Thinking

Computational thinking is that the process of approaching an issue systematically and creating and expressing an answer such it may be applied by a computer. But you do not have to be a scientist to think sort of a computer scientist!

## Objectives

-   Introduction to computer science.
-   How computer can represent information from just a binary.
    -   Decimal to binary
-   Bit and byte.
-   Transistor
-   Representation from a character to binary. (ASCII and Unicode)
    -   Alphabet
    -   Emoticon 😂
    -   Other languages, such as Chinesse.
    -   Colour (RGB)
    -   Video, eg. GIF
-   Algorithm
    -   How to comapare how much better the alogirthm is  from many other examples.
-   Programming languages
    -   Pseude code
    -   Scratch
        -  Hello World
        -  Scarcth example projects


## Introduction
What is then computer science? I dare we can describe computer science as this. It is just the process of solving problems. What does it mean to solve a problem. You have got some input and the goal is to get some output, the solution in that particular problem. In between really is computer science.

As we begin to fill in some of those blanks, when we consider a problem to be solved, we have all to agree how to represent something, especially using machines. How are we going to represent these inputs and outputs? So the first concept we explore in computer science is how computer represent information itself. What language does the computer speak?

## Representation
Binary is numbers using only two symbols: 1 (one) and 0 (zero). When you are sending a video, image and alphabet or an emoticon, at the end of the day you are actually sending an binary. How can we convert this decimal into binary? or a colour,alphabet,etc into binary?

### Decimal
Dec means ten and decimal uses the ten symbol from 0 to 9.
```
0 1 2 3 4 5 6 7 8 9
```

### Binary
```
0 1
```
![This counter shows how to count in binary from numbers zero through thirty-one.](../images/computionalThinking/220px-Binary_counter.gif)
ref. https://en.wikipedia.org/wiki/Binary_number#/media/File:Binary_counter.gif


### Alphabet
ASCII (American Standard Code for Information Interchange)

Unicode

### Colour
RGB (Red, Green, Blue)

### Emoticon 😂
😂
binary	 = 11111011000000010
decimal  = 128,514

### Video
<iframe src="https://giphy.com/embed/l4Jz3a8jO92crUlWM" width="480" height="375" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/meme-steak-seasoning-l4Jz3a8jO92crUlWM">via GIPHY</a></p>

GIF(Graphic Interchange Format)
noun-COMPUTING
a lossless format for image files that supports both animated and static images.

# Algorithm
Algorithm is step by step process to solve a problem.
## Problem
This is an old school problem, where you might have lots and lots of names and lots of lots of numbers, and those names are hopefully sorted alphabetically from A through Z in a phone book. Eventhough, most of us doesn't have the same problem to be considered, this is actually the same as your iPhone or Android phone which has of all your contact from top to bottom.
## 1st Algorithm
Let's consider a simple approach. We are going to look at the first page and look for someone on the phone book. Suppose that person's name is Jeff. He is not here and we turn the page again, again and again, untill we find Jeff. This is an algorithm. But what would you say? Is this algorithm correct? Yeah I mean it's pretty slow. It's pretty stupid because it is going to take forever, like hundreds page to find Jeff.

## 2st Algorithm
Now, instead of one and find Jeff on that page. We are going to turns it 2 pages each and find Jeff. This algorithm might has a problem because you might skip Jeff.

## 3rd Algorithm


How much better those algorithm are?
The y axis or vertical axis is the time to solve some problem, and the horizontal axis is the size of the problem.

# Programming Language

## Pseude Code
Pick up phone book
Open to middle of phone book
Look at page
If Nancy is on page
 	Call Nancy
Else if Nancy is earlier in book
 	Open to middle of left half of book
 	Go back to line 3
Else if Nancy is later in book
 	Open to middle of right half of book
 	Go back to line 3
Else
 	Quit


## Scratch
[scratch.mit.edu](https://scratch.mit.edu/)


# Useful Links and Reference