# 4. 2D Design

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Objectives

-   Vector vs pixle
-   10 things to know for beginners (Adobe Illustrator)


# Vector vs Pixle

Vector - resolution independent
Pixle  - photoshop

## 1. Profile
- Print - colour seperated (flyer)
- Web - web work (screen size)
- Devices - application for your iPad or Android tablet or smartphone
- Video and film - logo that you are going to animate in adobe after effects or put it in adobe premiere

## 2. Zoom
zoom+alt is to minimize

Windows: press & hold ctrl + Press 0 is to zoom to fit canvas to worspace
Mac : press & hold cmd + Press 0 is to zoom to fit canvas to worspace

## 3. Manouvering
Move around composition area: Press & hold space bar + click & drag

## 4. Selection tool
![](../images/2dDesign/selectionTool.png)

Selection tool is to shift the object and rotate.

Direct selection tool - press shift to keep the object propotional while reshaping.

## 5. Dupliate
Hold down alt key with selection tool and drag.

## 6. Colour
![](../images/2dDesign/color.png)

## 7. Select Multiple Objects
Select by the selection tool 
hold shift to select multiple object

## 8. Aligning

window- align panel

## 9. Ruler
command +R

## 10. Export
File- export - jpeg, photoshop file,etc
